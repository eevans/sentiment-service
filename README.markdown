# Sentiment analysis service

Sentiment analysis data for a Wikimedia talk pages.

## Disclaimer / Setting expectations

This service was put together as part of a week long hackathon project for the Platform Engineering Team.
The analysis this service provided supplied input to a simple 2d video game. In the game, nuts (representing
positively scored comments), and rocks (representing negative comments) fell from the sky. A user-controlled
squirrel below, moved left and right to catch the falling nuts, while avoiding rocks.

The scoring service utilizes a pretrained analyzer called VADER. VADER is meant for the sort of language
found on social media, with short sentences, some slang, and abbreviations; It is not ideal for the sort of
language used on talk pages.

Scores are returned as an array, in the order they appear (top-to-bottom), while traversing the graph of
replies. This isn't expected to be generally useful, it was meant as input for the aforementioned game.

## Dependencies

- [mwchatter][mwchatter]
- [NLTK][nltk] (`pip install nltk`)
- [requests][requests] (`pip install requests`)
- [flask][flask] (`pip install flask`)

NLTK also needs some additional data:

```sh-session
$ python3 -c "import nltk; nltk.download('vader_lexicon', download_dir='.')"
$ python3 -c "import nltk; nltk.download('punkt', download_dir='.')"
```

## Running

```sh-session
$ flask run
...
```

## API

### `GET /scored/{project}/{lang}/{title}`

Returns a JSON object with a `scores` attribute, an array containing the sentiment scores of each comments.

```sh-session
$ http http://127.0.0.1:5000/wikipedia/en/Banana
HTTP/1.0 200 OK
Content-Length: 113
Content-Type: application/json
Date: Wed, 16 Feb 2022 01:14:58 GMT
Server: Werkzeug/2.0.3 Python/3.9.10

{
    "scores": [
        0.04228571428571428,
        0.0,
        -0.23591428571428572,
        -0.24029999999999999,
        -0.021879999999999997,
        0.0,
        0.21075
    ]
}


$
```

### `GET /aggregated/{project}/{lang}/{title}`

Returns a JSON object with a `score` attribute, an aggregated value of the sentiment scores for a page.

```sh-session
$ http http://localhost:5000/aggregated/wikipedia/en/Banana
HTTP/1.0 200 OK
Content-Length: 31
Content-Type: application/json
Date: Wed, 16 Feb 2022 19:51:28 GMT
Server: Werkzeug/2.0.3 Python/3.9.10

{
    "score": -0.24505857142857146
}


$
```

### `GET /score_detailed/{project}/{lang}/{title}`

Returns a JSON object with a `scores` attribute, an array of objects corresponding to comments. Each object
contains a list sentences with accompanying score (`sentences`), and the `mean` of all sentences.

```sh-session
$ http http://localhost:5000/score_detailed/wikipedia/en/Banana
HTTP/1.0 200 OK
Access-Control-Allow-Origin: *
Content-Length: 5213
Content-Type: application/json
Date: Fri, 18 Feb 2022 19:46:46 GMT
Server: Werkzeug/2.0.3 Python/3.9.10

{
    "scores": [
        {
            "mean": 0.04228571428571428,
            "sentences": [
                {
                    "mean": 0.0,
                    "sentences": [
                        {
                            "score": 0.0,
                            "text": "Wiki Education Foundation-supported course assignment"
                        },
                        {
                            "score": 0.0,
                            "text": "File:Sciences humaines.svg40px This article was the subject of a Wiki Education Foundation-supported course assignment, between spanclassmw-formatted-datetitle2019-01-077 January 2019span and spanclassmw-formatted-datetitle2019-04-099 April 2019span."
                        },
                        {
                            "score": 0.0,
                            "text": "Further details are available Wikipedia:Wiki_Ed/McMaster_University/Global_Change,_Ecosystems_and_the_Earth_System_ENVIRSC_3BO3_(Winter)on the course page."
                        },
                        {
                            "score": 0.0,
                            "text": "Peer reviewers: User:GyaredosGyaredos."
                        },
                        {
                            "score": 0.0,
                            "text": "smallAbove undated message substituted from Template:Dashboard.wikiedu.org assignment by User:PrimeBOTPrimeBOT (User talk:PrimeBOTtalk) 15:15, 16 January 2022 (UTC)"
                        }
                    ]
                },
                ...
        }
    ]
}
```

[mwchatter]: https://github.com/mediawiki-utilities/python-mwchatter
[nltk]: https://www.nltk.org/install.html
[requests]: https://docs.python-requests.org/en/latest/
[flask]: https://palletsprojects.com/p/flask/
